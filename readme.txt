=== Automatic Header IDs ===
Contributors: simonw42
Tags: anchors, headings
Requires at least: 6.2.0
Tested up to: 6.4
Requires PHP: 7.4.0
Stable tag: 1.0.0
License: GPL-2.0-or-later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Automatically generate IDs for header tags in your page content.

== Description ==
Being able to link directly to sections within your content requires those
sections to have IDs. Manually creating IDs, especially in longer pages and
posts, can get tedious. This plugin will automatically generate IDs for you.

It works by looking at any `h1`–`h6` tags and, if the tag does not already have
an ID, generating one from the content of the tag.

As long as you're using `the_content()` or the `wp:post-content` block, headers
will automatically receive IDs once the plugin is installed and activated.

Support is available by creating an issue in [Gitlab](https://gitlab.com/simon_w/auto-header-ids/-/issues).
