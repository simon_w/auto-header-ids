<?php
/**
 * Automatic Header IDs
 *
 * @package           auto-header-ids
 * @author            Simon Welsh
 * @copyright         2019 Simon Welsh
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Automatic Header IDs
 * Description:       Automatically generates IDs for headers that don't have one set.
 * Version:           1.0.0
 * Requires at least: 6.2.0
 * Requires PHP:      7.4
 * Author:            Simon Welsh
 * Author URI:        https://coding-aloud.nz
 * Text Domain:       auto-header-ids
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

/*
Automatic Header IDs is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 2 of the License, or any later version.

Automatic Header IDs is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
Automatic Header IDs. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

if ( ! function_exists( 'auto_header_ids_hook_content' ) ) :
	/**
	 * Generate IDs for header tags in the content
	 *
	 * @param string $content The content to process.
	 */
	function auto_header_ids_hook_content( string $content ): string {
		$known_ids = array();

		/*
		 * We loop through the content twice.
		 *
		 * The first pass uses `WP_HTML_Tag_Processor` to pull out any existing IDs,
		 * so we don't duplicate them.
		 *
		 * The second pass then uses `preg_replace_callback` to generate IDs for
		 * any header that doesn't already have one.
		 */

		$tags = new WP_HTML_Tag_Processor( $content );
		while ( $tags->next_tag() ) {
			$id = $tags->get_attribute( 'id' );
			if ( $id ) {
				$known_ids[] = $id;
			}
		}

		/*
		 * This regex matches:
		 * - The start of an opening `h` tag (`h1`–`h6`)
		 * - Everything up to the next `>`
		 * - A literal `>`
		 * - The least amount of content
		 * - The closing `h` tag, using a back-ref to explicitly match the same
		 *   number that was used in the opening tag.
		 */
		$content = \preg_replace_callback(
			'#(?<open><h([1-6]))(?<attrs>.*?)>(?<content>.*?)(?<close></h\2>)#simu',
			function ( $matches ) use ( &$known_ids ) {
				if ( \str_contains( \strtolower( $matches['attrs'] ), ' id=' ) ) {
					return $matches[0];
				}

				$slug = sanitize_title( $matches['content'], 'header' );

				$id = $slug;
				$at = 1;

				while ( \in_array( $id, $known_ids, true ) ) {
					$id = $slug . '-' . $at;
					$at++;
				}

				$known_ids[] = $id;

				return \sprintf(
					'%s id="%s"%s>%s%s',
					$matches['open'],
					$id,
					$matches['attrs'],
					$matches['content'],
					$matches['close'],
				);
			},
			$content,
		);

		return $content;
	}

	add_filter( 'the_content', 'auto_header_ids_hook_content', 99 );
endif;
